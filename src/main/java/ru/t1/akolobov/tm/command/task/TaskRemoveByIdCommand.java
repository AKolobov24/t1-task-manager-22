package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-id";

    public static final String DESCRIPTION = "Find task by Id and remove.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getTaskService().removeById(userId, id);
    }

}
