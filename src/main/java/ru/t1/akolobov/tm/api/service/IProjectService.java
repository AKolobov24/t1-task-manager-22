package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    List<Project> findAll(String userId, Sort sort);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, Status status);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

}
