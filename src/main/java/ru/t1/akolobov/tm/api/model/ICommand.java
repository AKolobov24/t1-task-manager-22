package ru.t1.akolobov.tm.api.model;

import ru.t1.akolobov.tm.enumerated.Role;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();

    void execute();

    Role[] getRoles();

}
