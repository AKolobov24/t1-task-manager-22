package ru.t1.akolobov.tm.api.repository;

import ru.t1.akolobov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(final AbstractCommand command);

    AbstractCommand getCommandByName(final String name);

    AbstractCommand getCommandByArgument(final String argument);

    Collection<AbstractCommand> getTerminalCommands();

}
