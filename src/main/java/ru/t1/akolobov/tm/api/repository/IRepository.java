package ru.t1.akolobov.tm.api.repository;

import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    boolean existById(String id);

    List<M> findAll();

    List<M> findAll(Comparator<? super M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    Integer getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}
