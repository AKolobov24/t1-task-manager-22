package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    List<Task> findAll(String userId, Sort sort);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, Status status);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

}
