package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.IUserOwnedRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) return null;
        model.setUserId(userId);
        models.add(model);
        return model;
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<? super M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()) && userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public Integer getSize(final String userId) {
        return findAll(userId).size();
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty() || model == null) return null;
        return removeById(userId, model.getId());
    }

    private void removeAll(final List<M> models) {
        if (models.isEmpty()) return;
        models.forEach(this.models::remove);
    }

    @Override
    public M removeById(final String userId, final String id) {
        final M model = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

}
