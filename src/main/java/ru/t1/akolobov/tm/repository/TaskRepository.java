package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
    }

}
